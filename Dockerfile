FROM node:14

ENV APPLICATION_INSTANCE=example

WORKDIR /app

RUN npm install

COPY . .

EXPOSE 80

CMD ["node", "src/count-server.js"]