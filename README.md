# Simple NodeJS Webapp

The application uses an environment variable `APPLICATION_INSTANCE`.  
This variable will be used by the application to listen on a specific path (i.e. `/${application_instance}/health`).

## Launch the application:

```bash
export APPLICATION_INSTANCE=example
node src/count-server.js
```

DockerHub container: docker pull maxymshyjko988/shyiko_exam

Minikube commands:
minikube start
kubectl create deployment shyiko-exam --image=maxymshyjko988/shyiko_exam:0.1
kubectl expose deployment shyiko-exam --type=NodePort --port=8080
kubectl scale deployments shyiko-exam --replicas=4

C:\Users\maxym>kubectl get deployments
NAME          READY   UP-TO-DATE   AVAILABLE   AGE
shyiko-exam   4/4     4            4           2m47s

Q1: Define what is a Microservices architecture and how does it differ
from a Monolithic application.
	Microservice architecture is a type of orgaising the project, in which it is splitted in a small units, which are responsible for a certain domain of functions. This means that each separate unit can execute only limited amount of tasks and it can communicate with other services to ask for some computations or send the result or notification. Contrary, monolithic applications are just one huge block of code with all of the functionality inside one package

Q2: Compare Microservices and Monolithic architectures by listing their respective pros and cons.
	Speaking about microservices architecture, it can be said that such programs are easy to scale, easy to test and write the source code due to the fact that the tasks can be easily assigned to different teams and it is needed to write smaller pieces of code per service. Also, it is more reliable architecture, since if one service is not responding, the app in general is still working. 
On the other hand monolithic applications demand really much attention from the developers, because the final product is very large and it is much harder to write in a team. Scaling of such program is also a problem due to its size. However, since it's just one chunk of code, it's much easier to deploy and design. 

Q3: In a Micoservices architecture, explain how should the application be split and why.
	Appliction should be split by domains of functionality. If we want to split a monolithic app, we would not want to export each different function to a separate container. It is much better to group the functions by the specific tasks they are addressing. If we have an app like spotify, we will not make separate containers for the button to pause, play a song, but it would rather be a block called music control.
Q4: Briefly explain the CAP theorem and its relevance to distributed
systems and Microservices.
	In distributed systems and microservices, exists a problem with messaging between the services. Since the connection can be lost, the data also can be lost. Because of this the software might be unavailable, so the users will need to wait for the system to reestablish connections and send the data. Another solution is to kill the service and restart is instantly with the loss of data. This way user will not get the data as soon as other users. This takes us to the CAP theorem, which says that with Partitioned systems, software can be either Consistent or Available, but not both
Q5: What consequences ?? on the architecture ?
Architecture determines the structure of the code, which means it's just a specification used to work on a program. It determines which services are there, what they are responsible for and how to they communicate between each other

Q6: Provide an example of how microservices can enhance scalability in
a cloud environment.
Main two approacher to scale a service is so called vertical and horizontal scaling. Vertical scaling is about adding more resources for the machine, which is not really interesting. Horizontal scaling is achieved by creating a several instances of containers to achieve more availability. In microservices it is possible to duplicate the container and balance the workload. This way, if any of the containers is down, the service will still be able to respond through the replicas

Q7: What is statelessness and why is it important in microservices architecture ?
Statelessneess means that any of the requests between containers do not store any user information, which is useful in case the software is not responding it can be killed and restarted easily without loss of data.

Q8: What purposes does an API Gateway serve ?
It is basically a proxy server, which can be used for load balancing in the node cluster.
